# Use node:alpine for stage of build named as builder
FROM node:alpine as builder
WORKDIR '/app'
COPY package.json .
RUN npm install
COPY . . 
RUN npm run build

# on the next step we need only folder /app/build from previous step
# next stage is beginning by the next line
FROM nginx

# Copy all from /app/build folder from the builder stage to the folder on new container
COPY --from=builder /app/build /usr/share/nginx/html

# we do not need use CMD for default command because nginx image make this step for us
